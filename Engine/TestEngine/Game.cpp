#include "Game.h"
#define CAMERASPEED_1 1.0f
#define CAMERASPEED_2 0.01f

Game::Game()
{
}

Game::~Game()
{

	delete testCoso;
	delete city;
	if (treeTest)
		delete treeTest;
	delete cameraCoso;
	

}

bool Game::OnStart()
{
	camera = new NewCamera(GetRenderer());

	GetRenderer()->SetPerspective();
	GetRenderer()->MoveCamera(glm::vec3(0, 0, 100));
	camera->Strafe(0, 0);

	input = Input::getInstance();
	input->SetWindowContext(GetWindow());

	material = new Material();
	material->LoadShader("Shaders\\SimpleVertexShader.vertexshader"		// Vertex Shader
					   , "Shaders\\SimpleFragmentShader.fragmentshader"	// Fragment Shader
	);

	testCoso = new Coso(GetRenderer(), "Test");
	testCoso->AddComponent(new MeshRenderer(testCoso->GetTransform(),GetRenderer(),material,"StarShip.obj","default.bmp"));
	testCoso->GetTransform()->Scale(10, 10, 10);
	testCoso->GetTransform()->Teleport(0, 0, -5000);
	city = new Coso(GetRenderer(), "City");
	city->AddComponent(new MeshRenderer(city->GetTransform(), GetRenderer(), material, "city.obj", "city.bmp"));
	city->GetTransform()->Teleport(-1000, -400, -1000);
	city->GetTransform()->Scale(2000, 2000, 2000);
	hijito = new Coso(GetRenderer(), "Hijito");
	hijito->AddComponent(new MeshRenderer(hijito->GetTransform(), GetRenderer(), material, "uman.obj", "default.bmp"));
	testCoso->AddChild(hijito);

	TreeModel* tm = new TreeModel((string)"Zenos.obj", (string)"city.bmp", GetRenderer(),material);
	treeTest = tm->GetGeneratedCoso();
	delete tm;
	treeTest->GetTransform()->Scale(4000, 4000, 4000);
	cameraCoso = new Coso(GetRenderer(), "Camera");
	cameraCoso->AddComponent(new CameraComponent(cameraCoso->GetTransform(), GetRenderer()));

	return true;
}

void Game::FillAsteroidsData()
{
	
}

bool Game::OnStop()
{

	return true;
}

bool Game::OnUpdate()
{
	CameraComponent* camcomp = (CameraComponent*)(cameraCoso->GetComponent("CameraComponent"));

		if (input->isInput(GLFW_KEY_SPACE))
			//camera->Strafe(0, CAMERASPEED_1);
			camcomp->Strafe(0, CAMERASPEED_1);

		if (input->isInput(GLFW_KEY_LEFT_CONTROL))
			//camera->Strafe(0, -CAMERASPEED_1);
			camcomp->Strafe(0, -CAMERASPEED_1);

		if (input->isInput(GLFW_KEY_Q))
			//camera->Strafe(-CAMERASPEED_1, 0);
			camcomp->Strafe(-CAMERASPEED_1, 0);

		if (input->isInput(GLFW_KEY_E))
			//camera->Strafe(CAMERASPEED_1,0);
			camcomp->Strafe(CAMERASPEED_1, 0);

		if (input->isInput(GLFW_KEY_W))
			//camera->Walk(CAMERASPEED_1);
			camcomp->Walk(CAMERASPEED_1);

		if (input->isInput(GLFW_KEY_S))
			//camera->Walk(-CAMERASPEED_1);
			camcomp->Walk(-CAMERASPEED_1);

		if (input->isInput(GLFW_KEY_D))
			//camera->Yaw(-CAMERASPEED_2);
			camcomp->Yaw(-CAMERASPEED_2);
		
		if (input->isInput(GLFW_KEY_A))
			//camera->Yaw(CAMERASPEED_2);
			camcomp->Yaw(CAMERASPEED_2);

		if (input->isInput(GLFW_KEY_LEFT))
			//camera->Roll(-CAMERASPEED_2);
			camcomp->Roll(-CAMERASPEED_2);

		if (input->isInput(GLFW_KEY_RIGHT))
			//camera->Roll(CAMERASPEED_2);
			camcomp->Roll(CAMERASPEED_2);

		if (input->isInput(GLFW_KEY_UP))
			//camera->Pitch(CAMERASPEED_2);
			camcomp->Pitch(CAMERASPEED_2);

		if (input->isInput(GLFW_KEY_DOWN))
			//camera->Pitch(-CAMERASPEED_2);
			camcomp->Pitch(-CAMERASPEED_2);

		if (input->isInput(GLFW_KEY_1))
			//camera->StrafeAdvance(CAMERASPEED_1);
			camcomp->StrafeAdvance(CAMERASPEED_1);

		if (input->isInput(GLFW_KEY_2))
			//camera->StrafeAdvance(-CAMERASPEED_1);
			camcomp->StrafeAdvance(-CAMERASPEED_1);
		
		input->PollEvents();

		testCoso->Update();
		city->Update();
		testCoso->GetTransform()->Translate(0, 0, 1);
		treeTest->Update();
		cameraCoso->Update();

		return true;

}

bool Game::OnDraw()
{
	return true;
}

void Game::Restart()
{

}