#pragma once

#include "GameBase.h"
#include "Triangle.h"
#include "Rectangle.h"
#include "Circle.h"
#include "Sprite.h"
#include "Tilemap.h"
#include "Input.h"
#include "CollisionManager.h"
#include "Player.h"
#include "NewCamera.h"
#include "Asteroid.h"
#include "Cube.h"
#include "Mesh_NoAssimp.h"
#include "Mesh.h"
#include "Model.h"
#include "ModelImporter.h"
#include "Coso.h"
#include "Transform.h"
#include "MaterialComponent.h"
#include "MeshRenderer.h"
#include "Componente.h"
#include "CameraComponent.h"
#include "TreeModel.h"

enum GameState
{
	LOSE = -1,
	CONTINUE,
	WIN
};

class Game : public GameBase
{
	Input* input;
	Material* material;
	Material* matTexture;
	Player* player;
	list<Asteroid*>* asteroids;
	Tilemap* tilemap;
	NewCamera* camera;
	Cube* dubidu;
	Rectangle* rectangulo;
	Mesh_NoAssimp* uman;
	Mesh* gooduman;
	Mesh* elSegundoMesh;
	Coso* testCoso;
	Coso* city;
	Coso* hijito;
	Coso* treeTest;
	Coso* cameraCoso;

	float speed;		// Speed

	const int totalAsteroids = 25;

	GameState gameState;

	void FillAsteroidsData();
	void Restart();

protected:
	bool OnStart() override;
	bool OnStop() override;
	bool OnUpdate() override;
	bool OnDraw() override;

public:
	Game();
	~Game();
};

