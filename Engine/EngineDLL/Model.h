#pragma once

#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include <vector>
#include "TextureImporter.h"
#include "Exports.h"
#include "ModelImporter.h"
#include "assimp\Importer.hpp"
#include "assimp\postprocess.h"
#include "assimp\scene.h"
#include "GL\glew.h"
#include "GLFW\glfw3.h"
#include "glm\glm.hpp"
#include "glm\glm.hpp"
#include "Coso.h"


class ENGINEDLL_API Model
{
	Model();
	~Model();
	static Model *instance; //esta instancia de Model
	aiScene* scene; //la escena que contiene el archivo
	Assimp::Importer importer; //el importer de Assimp
	void InitFromScene(const aiScene* scene, const char* texpath, vector<MeshEntry>& vecMeshEntry, vector<Header>& textures, Renderer* rnd);
	void InitMesh(unsigned int index, const aiMesh* mesh, vector<MeshEntry>& meshEntryVec, Renderer* rnd);

public:
	void LoadMesh(const string& modelPath, string& texPath, vector<MeshEntry>& meshEntriesVec, vector<Header>& textures, Renderer* rnd);
	static Model* GetInstance();
};
