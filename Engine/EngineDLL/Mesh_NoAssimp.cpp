#include "Mesh_NoAssimp.h"
#pragma warning(disable:4996)

Mesh_NoAssimp::Mesh_NoAssimp(char* _path, Renderer* renderer, Material* material, Layers tag):Shape(renderer, material, tag)
{
	if (!loadOBJ(_path, vertices, uvs, normals)) {
		cout << "Error loading Mesh File, Mesh wont be displayed." << endl;
	}
}


Mesh_NoAssimp::~Mesh_NoAssimp()
{
}

bool Mesh_NoAssimp::loadOBJ(const char * path, std::vector < glm::vec3 > & out_vertices, std::vector < glm::vec2 > & out_uvs, std::vector < glm::vec3 > & out_normals)
{
	std::vector< unsigned int > vertexIndices, uvIndices, normalIndices;
	std::vector< glm::vec3 > temp_vertices;
	std::vector< glm::vec2 > temp_uvs;
	std::vector< glm::vec3 > temp_normals;

	FILE * file;
	fopen_s(&file,path, "r");
	if (file == NULL) {
		printf("Impossible to open the file !\n");
		return false;
	}

	while (1) {

		char lineHeader[128];
		// Lee la primera palabra de la l�nea
		int res = fscanf(file, "%s", lineHeader);
		if (res == EOF)
			break; // EOF = End Of File, es decir, el final del archivo. Se finaliza el ciclo.

				   // else : analizar el lineHeader
		if (strcmp(lineHeader, "v") == 0) {
			glm::vec3 vertex;
			fscanf_s(file, "%f %f %f\n", &vertex.x, &vertex.y, &vertex.z);
			temp_vertices.push_back(vertex);

		}
		else if (strcmp(lineHeader, "vt") == 0) {
			glm::vec2 uv;
			fscanf_s(file, "%f %f\n", &uv.x, &uv.y);
			temp_uvs.push_back(uv);

		}
		else if (strcmp(lineHeader, "vn") == 0) {
			glm::vec3 normal;
			fscanf_s(file, "%f %f %f\n", &normal.x, &normal.y, &normal.z);
			temp_normals.push_back(normal);

		}
		else if (strcmp(lineHeader, "f") == 0) {
			std::string vertex1, vertex2, vertex3;
			unsigned int vertexIndex[3], uvIndex[3], normalIndex[3];
			int matches = fscanf(file, "%d/%d/%d %d/%d/%d %d/%d/%d\n", &vertexIndex[0], &uvIndex[0], &normalIndex[0], &vertexIndex[1], &uvIndex[1], &normalIndex[1], &vertexIndex[2], &uvIndex[2], &normalIndex[2]);
			if (matches != 9) {
				printf("File can't be read by our simple parser : ( Try exporting with other options\n");
				return false;
			}
			vertexIndices.push_back(vertexIndex[0]);
			vertexIndices.push_back(vertexIndex[1]);
			vertexIndices.push_back(vertexIndex[2]);
			uvIndices.push_back(uvIndex[0]);
			uvIndices.push_back(uvIndex[1]);
			uvIndices.push_back(uvIndex[2]);
			normalIndices.push_back(normalIndex[0]);
			normalIndices.push_back(normalIndex[1]);
			normalIndices.push_back(normalIndex[2]);

		}
	}
	for (unsigned int i = 0; i < vertexIndices.size(); i++) {
		unsigned int vertexIndex = vertexIndices[i];
		glm::vec3 vertex = temp_vertices[vertexIndex - 1];
		out_vertices.push_back(vertex);
	}
	for (unsigned int i = 0; i < uvIndices.size(); i++) {
		unsigned int uvIndex = uvIndices[i];
		glm::vec3 vertex = temp_vertices[uvIndex - 1];
		out_uvs.push_back(vertex);
	}
	for (unsigned int i = 0; i < normalIndices.size(); i++) {
		unsigned int normalIndex = normalIndices[i];
		glm::vec3 vertex = temp_vertices[normalIndex - 1];
		out_normals.push_back(vertex);
	}
}

void Mesh_NoAssimp::Draw() 
{
	bufferId = SetVertices(vertices, vertices.size());

	renderer->loadIdentityMatrix();
	renderer->SetModelMatrix(model);

	if (material != NULL) {
		material->Bind();
		material->SetMatrixProperty("WVP", renderer->GetMVP());
	}
	renderer->EnableAttributes(0);
	//renderer->EnableAttributes(1);
	renderer->BindBuffer(bufferId, 0);
	//renderer->BindBuffer(colorBufferId, 1);
	renderer->BindMeshBuffer(indexBufferId);
	//renderer->DrawIndex(indexVCount);
	renderer->DisableAttributes(0);
//	renderer->DisableAttributes(1);
}

void Mesh_NoAssimp::DrawMesh(int type) 
{
	DrawMesh(GL_TRIANGLES);
	std::cout << "Drawing Mesh (No Assimp)" << std::endl;
}