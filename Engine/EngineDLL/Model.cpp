#include "Model.h"

Model * Model::instance = NULL;

Model::Model()
{
}

Model::~Model()
{
}

Model* Model::GetInstance() //Devuelve la instancia actual del modelo.
{
	if (instance == NULL) //si no existe...
		instance = new Model(); //lo crea
	return instance;
}

void Model::LoadMesh(const string& modelPath, string& texpath, vector<MeshEntry>& meshEntryVec, vector<Header>& textures, Renderer* rnd)
{
	Assimp::Importer importer; //Creo el importer de Assimp
	const aiScene* scene = importer.ReadFile(
		modelPath.c_str(),
	aiProcess_Triangulate | aiProcess_GenSmoothNormals | aiProcess_JoinIdenticalVertices
	); //cargo la escena en el archivo con el importer
	
	if (scene) //Si se obtuvo algo
		InitFromScene(scene, texpath.c_str(), meshEntryVec, textures, rnd); //inicializo los meshes dentro de la escena
	else //sino hay un error
		cout << "Error Loading Mesh, Mesh wont be displayed." << endl; //error
}

void Model::InitFromScene(const aiScene* scene, const char* texpath, vector<MeshEntry>& vecMeshEntry, vector<Header>& textures, Renderer* rnd)
{
	vecMeshEntry.resize(scene->mNumMeshes); //Seteo el tamanio de los contenedores para la cantidad de componentes dentro de la escena.
	textures.resize(scene->mNumMaterials);

	for (int i = 0; i < vecMeshEntry.size(); i++) //por cada uno de los meshes en la escena
		InitMesh(i, scene->mMeshes[i], vecMeshEntry, rnd); //mando el mesh a inicializar

	for (int i = 0; i < scene->mNumMaterials; i++) //por cada uno de los materiales en la escena
		textures[i] = TextureImporter::loadBMP_custom(texpath); //cargo una textura en el
}

void Model::InitMesh(unsigned int index, const aiMesh* mesh, std::vector<MeshEntry>& meshEntriesVec, Renderer* rnd) //inicializacion de los meshes individuales.
{
	std::vector<Vertex> Vertices; //para guardar los vertices del modelo
	std::vector<unsigned int> Indices; //para guardar los indices del modelo

	aiVector3D Zero3D(0.0f, 0.0f, 0.0f); //No se, estaba en el tuto. calculo que para inicializar.

	for (int i = 0; i < mesh->mNumVertices; i++) //por cada uno de los vertices...
	{
		aiVector3D* vectexCoord = mesh->HasTextureCoords(0) ? &(mesh->mTextureCoords[0][i]) : &Zero3D; //guardo las coordenadas de la textura. (En el tuto usan ese "?" que ni idea de que es, asi que no puedo simplificar esta funcion)

		Vertex v(vec3((mesh->mVertices[i]).x, (mesh->mVertices[i]).y, (mesh->mVertices[i]).z), //Guardo posiciones del vertice
			vec2((float)vectexCoord->x, (float)vectexCoord->y),
			vec3((float)(mesh->mNormals[i]).x, (float)(mesh->mNormals[i]).y, (float)(mesh->mNormals[i]).z)); //Guardo las normales
		// Creacion de un vertice que contiene todos los datos extraidos

		Vertices.push_back(v); //Se guarda en el Vector el vertice completo.
	}

	for (int i = 0; i < mesh->mNumFaces; i++) //Esto es para las F que se rompian en el otro importer que no sirve para nada.
	{ //Datos de las caras
		aiFace& Face = mesh->mFaces[i];
		assert(Face.mNumIndices == 3);
		Indices.push_back(Face.mIndices[0]);
		Indices.push_back(Face.mIndices[1]);
		Indices.push_back(Face.mIndices[2]);
	}

	meshEntriesVec[index].Init(Vertices, Indices, rnd);
}