#include "Shape.h"

Shape::Shape(Renderer* renderer, Material* material, Layers tag) : Entity(renderer, material, tag)
{
	srand(time(0));
	bufferId = -1;
	colorBufferId = -1;
}
Shape::~Shape()
{
}

void Shape::Update()
{
	Entity::Update();
}

void Shape::ShouldDispose()
{
	if (shouldDispose)
	{
		renderer->DestroyBuffer(bufferId);
		delete[] verticesData;
		shouldDispose = false;
	}
}

unsigned int Shape::SetVertices(float* vertices, int _count)
{
	verticesData = vertices;
	count = _count;

	unsigned int id = renderer->GenBuffer(verticesData, sizeof(float) * count * 3);
	shouldDispose = true;
	bufferId = id;
	return id;
}
unsigned int Shape::SetVertices(vector<vec3> vertices, int _count)
{
	vector<vec3> verticesData = vertices;
	count = _count;

	unsigned int id = renderer->GenBuffer(verticesData, verticesData.size());
	shouldDispose = true;
	bufferId = id;
	return id;
}
void Shape::SetColorVertices(float* vertices, int count)
{
	colorvertexcount = count;
	shouldDisposeColor = true;
	colorBufferId = renderer->GenColorBuffer(vertices, sizeof(float)* count * 3);
}

void Shape::SetIndexVertices(unsigned int * vertices, int count)
{
	indexVCount = count;
	shouldDisposeColor = true;
	indexBufferId = renderer->GenIndexBuffer(vertices, sizeof(unsigned int)* count);
}
void Shape::Dispose()
{
	if (shouldDispose)
	{
		renderer->DestroyBuffer(bufferId);
		shouldDispose = false;
	}
}
void Shape::DisposeColor()
{
	if (shouldDisposeColor)
	{
		renderer->DestroyBuffer(colorBufferId);
		shouldDisposeColor = false;
	}
}
