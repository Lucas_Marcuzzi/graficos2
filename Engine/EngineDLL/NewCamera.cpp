#include "NewCamera.h"
#include "glm\exponential.hpp"
#include "glm\glm.hpp"
#include "glm\ext.hpp"
#include "glm\gtx\transform.hpp"
#define GLM_ENABLE_EXPERIMENTAL

void NewCamera::Strafe(float x, float y)
{
	renderer->MoveCamera(glm::vec3(x, y, 0));
	eyePosition = renderer->GetCameraPosition();
	cameraPosition = renderer->GetRealCameraPosition();
}

NewCamera::NewCamera(Renderer* rnd) 
{
	renderer = rnd;

	//eyePosition = vec3(0.0f, 0.0f, 10.0f);
	eyePosition = renderer->GetCameraPosition();
	upVector = vec3(0.0f, 1.0f, 0.0f);


	forward = vec4(0.0f, 0.0f, -1.0f, 0.0f);
	right = vec4(1.0f, 0.0f, 0.0f, 0.0f);
	up = vec4(0.0f, 1.0f, 0.0f, 0.0f);

	cameraPosition = eyePosition + (vec3)forward;
}

void NewCamera::SetOrtho()
{
	renderer->SetOrtho();
}

void NewCamera::SetPerspective()
{
	renderer->SetPerspective();
}

void NewCamera::Walk(int ammount) {
	cameraPosition += (vec3)forward*ammount;
	eyePosition += (vec3)forward*ammount;
	renderer->UpdateViewMatrix(eyePosition, cameraPosition, up);
	renderer->SyncCameras(cameraPosition, eyePosition, up, forward);
}

void NewCamera::StrafeAdvance(int ammount) 
{
	renderer->MoveCamera(glm::vec3(0, 0, ammount));
	eyePosition = renderer->GetCameraPosition();
	renderer->SyncCameras(cameraPosition, eyePosition, up, forward);
}
void NewCamera::Pitch(float ammount)
{
	forward = glm::rotate(mat4(1.0f), ammount, vec3(right.x, right.y, right.z)) * forward;
	up = glm::rotate(mat4(1.0f), ammount, vec3(right.x, right.y, right.z)) * up;
	upVector = (vec3)up;
	cameraPosition = eyePosition + (vec3)forward;
	renderer->UpdateViewMatrix(eyePosition, cameraPosition, up);
	renderer->SyncCameras(cameraPosition, eyePosition, up, forward);
}
void NewCamera::Yaw(float ammount) 
{
	forward = glm::rotate(mat4(1.0f), ammount, vec3(upVector.x, upVector.y, upVector.z)) * forward;
	right = glm::rotate(mat4(1.0f), ammount, vec3(upVector.x, upVector.y, upVector.z)) * right;
	upVector = (vec3)up;
	cameraPosition = eyePosition + (vec3)forward;
	renderer->UpdateViewMatrix(eyePosition, cameraPosition, up);
	renderer->SyncCameras(cameraPosition, eyePosition, up, forward);
}
void NewCamera::Roll(float ammount)
{
	mat4 rot = rotate(mat4(1.0f), ammount, vec3(forward.x, forward.y, forward.z));
	right = rot * right;
	up = rot * up;
	upVector = (vec3)up;
	cameraPosition = eyePosition + (vec3)forward;
	renderer->UpdateViewMatrix(eyePosition, cameraPosition, up);
	renderer->SyncCameras(cameraPosition, eyePosition, up, forward);
}