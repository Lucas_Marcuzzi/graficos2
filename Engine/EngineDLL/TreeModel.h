#pragma once
#include "Componente.h"
#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include <vector>
#include "TextureImporter.h"
#include "Exports.h"
#include "ModelImporter.h"
#include "assimp\Importer.hpp"
#include "assimp\postprocess.h"
#include "assimp\scene.h"
#include "GL\glew.h"
#include "GLFW\glfw3.h"
#include "glm\glm.hpp"
#include "glm\glm.hpp"
#include "Coso.h"
#include "TreeMeshRenderer.h";

class ENGINEDLL_API TreeModel
{	
protected:
	Coso* generatedCoso;
public:
	TreeModel(const string& modelPath, string& texpath, Renderer* rnd, Material* _mtl);
	void GenerateTree(const aiScene* scene, Coso* _primerCoso, aiNode* root, const string& texturePath, Renderer* rnd, Material* _mtl);
	void InitMesh(const aiMesh* mesh, TreeMeshRenderer* _tmr, Renderer* rend);
	Coso* GetGeneratedCoso();
	~TreeModel();
};

