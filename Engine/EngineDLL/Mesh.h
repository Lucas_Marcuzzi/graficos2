#pragma once

#include "Shape.h"
#include "Definitions.h"
#include "ModelImporter.h"
#include "Model.h"
#include "TextureImporter.h"

class ENGINEDLL_API Mesh : public Shape
{
	char* path;
	vector<MeshEntry> vecMeshEntry;
	vector<Header> vecHeaders;
	vector<unsigned int> bufferTextureID;

public:
	Mesh(string _modelPath, string _texturePath, Renderer* _renderer, Material* material);
	~Mesh();
	void Draw() override; //el Render() del tuto, si no le pongo Draw flashea clase abstracta...
	void UpdateModel();
};