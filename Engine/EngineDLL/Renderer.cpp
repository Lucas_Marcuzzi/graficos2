#include "Renderer.h"

Renderer::Renderer()
{
}

Renderer::~Renderer()
{
}

bool Renderer::Start(Window* win)
{
	window = win;

	glfwMakeContextCurrent((GLFWwindow*)window->GetContext());

	
	glewExperimental = true;
	if (glewInit() != GLEW_OK)
	{
		fprintf(stderr, "Failed to initialize GLEW\n");
		glfwTerminate();
		return false;
	}

	// Habilidad el test de profundidad
	glEnable(GL_DEPTH_TEST);

	/*glEnable(GL_CULL_FACE);
	glCullFace(GL_FRONT);
	glFrontFace(GL_CW);*/
	//glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);

	// Aceptar el fragmento si est� m�s cerca de la c�mara que el fragmento anterior
	glDepthFunc(GL_LESS);

	glGenVertexArrays(1, &VertexArrayID);
	glBindVertexArray(VertexArrayID);

	projectionMatrix = glm::ortho(-512.0f, 512.0f, -384.0f, 384.0f, 0.0f, 100.0f);
	//projectionMatrix = glm::perspective(-90.0f, (float)(1024/768), 0.0f, 100.0f);

	cameraPosition = glm::vec3(0, 0, 3); //DONDE ESTA
	eyePosition = cameraPosition - vec3(0, 0, 3); //DONDE MIRA

	viewMatrix = glm::lookAt(
		cameraPosition,		// Camera is at (0, 0, 3), is World Space
		eyePosition,		// Looks at the origin
		up = glm::vec3(0, 1, 0)  // Head is up to (0, 1, 0)
	);

	return true;
}

bool Renderer::Stop()
{
	return true;
}

void Renderer::SetClearColor(float r, float g, float b, float a)
{
	glClearColor(r, g, b, a);
}

unsigned int Renderer::GenColorBuffer(float* buffer, int size)
{
	unsigned int colorbuffer;
	glGenBuffers(1, &colorbuffer);
	glBindBuffer(GL_ARRAY_BUFFER, colorbuffer);
	glBufferData(GL_ARRAY_BUFFER, size, buffer, GL_STATIC_DRAW);

	return colorbuffer;
}

unsigned int Renderer::GenIndexBuffer(unsigned int* buffer, int size)
{
	unsigned int idxbuffer;
	glGenBuffers(1, &idxbuffer);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, idxbuffer);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, size * sizeof(unsigned int), &buffer[0], GL_STATIC_DRAW);

	return idxbuffer;
}

unsigned int Renderer::GenIndexBuffer(std::vector<unsigned int> buffer)
{
	unsigned int idxbuffer;
	glGenBuffers(1, &idxbuffer);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, idxbuffer);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, buffer.size() * sizeof(unsigned int), &buffer[0], GL_STATIC_DRAW);

	return idxbuffer;
}


void Renderer::SwapBuffers()
{
	glfwSwapBuffers((GLFWwindow*)window->GetContext());
}

void Renderer::ClearScreen()
{
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
}

void Renderer::DestroyBuffer(unsigned int bufferId)
{
	glDeleteBuffers(1, &bufferId);
}

unsigned int Renderer::GenBuffer(float* buffer, int size)
{
	// Identificar el vertex buffer
	unsigned int vertexbuffer;
	// Generar un buffer, poner el resultado en el vertexbuffer que acabamos de crear
	glGenBuffers(1, &vertexbuffer);
	// Los siguientes comandos le dar�n caracter�sticas especiales al 'vertexbuffer' 
	glBindBuffer(GL_ARRAY_BUFFER, vertexbuffer);
	// Darle nuestros v�rtices a  OpenGL.
	glBufferData(GL_ARRAY_BUFFER, size, buffer, GL_STATIC_DRAW);

	return vertexbuffer;
}

unsigned int Renderer::GenBuffer(vector<vec3> buffer, int size)
{
	// Identificar el vertex buffer
	unsigned int vertexbuffer;
	// Generar un buffer, poner el resultado en el vertexbuffer que acabamos de crear
	glGenBuffers(1, &vertexbuffer);
	// Los siguientes comandos le dar�n caracter�sticas especiales al 'vertexbuffer' 
	glBindBuffer(GL_ARRAY_BUFFER, vertexbuffer);
	// Darle nuestros v�rtices a  OpenGL.
	glBufferData(GL_ARRAY_BUFFER, size * sizeof(glm::vec3), &buffer[0], GL_STATIC_DRAW);

	return vertexbuffer;
}

unsigned int Renderer::GenTexture(unsigned int width, unsigned int height, unsigned char* data)
{
	// Identificar el vertex buffer
	unsigned int vertexbuffer;
	glGenTextures(1, &vertexbuffer);

	// Se "Ata" la nueva textura : Todas las futuras funciones de texturas van a modificar esta textura
	glBindTexture(GL_TEXTURE_2D, vertexbuffer);

	// Se le pasa la imagen a OpenGL
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, width, height, 0, GL_BGR, GL_UNSIGNED_BYTE, data);

	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);

	return vertexbuffer;
}

unsigned int Renderer::GenTilemapTexture(unsigned int width, unsigned int height, unsigned char* data)
{
	unsigned int vertexbuffer;

	glGenTextures(1, &vertexbuffer);

	glBindTexture(GL_TEXTURE_2D, vertexbuffer);

	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, width, height, 0, GL_BGRA, GL_UNSIGNED_BYTE, data);

	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);

	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);

	return vertexbuffer;
}

void Renderer::EnableAttributes(unsigned int attributebId)
{
	glEnableVertexAttribArray(attributebId);
}

void Renderer::BindBuffer(unsigned int bufferId, unsigned int attributebId)
{
	glBindBuffer(GL_ARRAY_BUFFER, bufferId);
	glVertexAttribPointer(
		attributebId,       // debe corresponder en el shader
		3,                  // tama�o
		GL_FLOAT,           // tipo
		GL_FALSE,           // normalizado?
		0,                  // corrimiento
		(void*)0            // desfase del buffer
	);
}

void Renderer::BindMeshBuffer(unsigned int indexbuffer)
{
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, indexbuffer);
}

void Renderer::BindTextureBuffer(unsigned int bufferId, unsigned int attributebId)
{
	glBindBuffer(GL_ARRAY_BUFFER, bufferId);
	glVertexAttribPointer(
		attributebId,       // debe corresponder en el shader.
		2,                  // tama�o
		GL_FLOAT,           // tipo
		GL_FALSE,           // normalizado?
		0,                  // corrimiento
		(void*)0            // desfase del buffer
	);
}

void Renderer::DrawBuffer(unsigned int attributeId, int size, GLenum mode)
{
	glDrawArrays(mode, 0, size);
}

void Renderer::DrawIndex(int idxcount)
{
	glDrawElements(
		GL_TRIANGLES,
		idxcount,
		GL_UNSIGNED_INT,
		(void*)0
	);
}

void Renderer::DisableAttributes(unsigned int attributeId)
{
	glDisableVertexAttribArray(attributeId);
}

void Renderer::EnableBlend()
{
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
}

void Renderer::DisableBlend()
{
	glDisable(GL_BLEND);
}

void Renderer::MoveCamera(glm::vec3 newPos)
{
	cameraPosition += glm::vec3(newPos.x, newPos.y, newPos.z);
	eyePosition = cameraPosition + forward;

	viewMatrix = glm::lookAt(
		cameraPosition,
		eyePosition,
		up  // Head is up to (0, 1, 0)
	);
}


void Renderer::ResetCamera(float x, float y)
{
	glm::vec3 newPos = glm::vec3(x, y, 3);

	cameraPosition = glm::vec3(newPos.x, newPos.y, newPos.z);
	eyePosition = glm::vec3(0.0f, newPos.y, 0.0f);

	viewMatrix = glm::lookAt(
		cameraPosition,
		eyePosition,
		glm::vec3(0, 1, 0)  // Head is up to (0, 1, 0)
	);
}

void Renderer::UpdateViewMatrix(vec3 eyepos, vec3 camerapos, vec3 up) {
	viewMatrix = glm::lookAt(eyepos, camerapos, up);
}

void Renderer::loadIdentityMatrix()
{
	modelMatrix = glm::mat4(1.0f);

	SetMVP();
}

void Renderer::SetModelMatrix(glm::mat4 model)
{
	modelMatrix = model;

	SetMVP();
}


void Renderer::MultiplyModelMatrix(glm::mat4 model)
{
	modelMatrix *= model;

	SetMVP();
}

void Renderer::SetMVP()
{
	MVP = projectionMatrix * viewMatrix * modelMatrix;
}

glm::mat4& Renderer::GetMVP()
{
	return MVP;
}

void Renderer::SetCameraPosition(vec3 pos)
{
	cameraPosition = pos;
	eyePosition = cameraPosition + forward;

	viewMatrix = glm::lookAt(
		cameraPosition,
		eyePosition,
		up  // Head is up to (0, 1, 0)
	);
}

void Renderer::SetOrtho()
{
	projectionMatrix = glm::ortho(-512.0f, 512.0f, -384.0f, 384.0f, 0.0f, 1000.0f);
}

void Renderer::SetPerspective()
{
	projectionMatrix = glm::perspective(glm::radians(45.0f), (float)(1024 / 768), 0.1f, 100000.0f);
}

void Renderer::SyncCameras(vec3 _CameraPosition, vec3 _eyeposition, vec3 _up, vec3 _forward) 
{
	cameraPosition = _CameraPosition;
	eyePosition = _eyeposition;
	up = _up;
	forward = _forward;
}