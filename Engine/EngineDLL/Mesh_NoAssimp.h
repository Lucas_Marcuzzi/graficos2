#pragma once
#include <vector>
#include"glm\vec3.hpp"
#include "glm\vec2.hpp"
#include "Shape.h"

class ENGINEDLL_API Mesh_NoAssimp : Shape
{
private:
	std::vector< glm::vec3 > vertices;
	std::vector< glm::vec2 > uvs;
	std::vector< glm::vec3 > normals; // No las usaremos por ahora

public:
	Mesh_NoAssimp(char*_path, Renderer* renderer, Material* material, Layers tag);
	~Mesh_NoAssimp();
	bool loadOBJ(const char * path,
		std::vector < glm::vec3 > & out_vertices,
		std::vector < glm::vec2 > & out_uvs,
		std::vector < glm::vec3 > & out_normals);
	void Draw();
	void DrawMesh(int type);
};

