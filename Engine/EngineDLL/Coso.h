#pragma once
#include <vector>
#include "Exports.h"
#include "Componente.h"
#include "Transform.h"
#include "Renderer.h"

using namespace std;

class ENGINEDLL_API Coso
{
protected:
	vector<Componente*> componentes;
	vector<Coso*> hijos;
	Transform * transform;
	Coso* parent = nullptr;
	std::string id;
	Renderer* renderer;
	mat4 aux;
	mat4 identity;
public:
	Coso(Renderer* rnd, string _id);
	~Coso();
	void Update();
	void AddComponent(Componente* _component);
	void AddChild(Coso* _hijo);
	Componente* GetComponent(std::string _id);
	void SetParent(Coso* _parent);
	vector<Coso*> GetChildren();
	Coso* GetParent();
	Renderer* GetRenderer();
	Transform* GetTransform();
};