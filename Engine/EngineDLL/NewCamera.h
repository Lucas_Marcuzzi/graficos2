#pragma once
#include "Renderer.h"



class ENGINEDLL_API NewCamera {

private:
	Renderer * renderer;
	vec3 eyePosition;
	vec3 cameraPosition;
	vec3 upVector;
	vec4 forward;
	vec4 right;
	vec4 up;
public:
	void Strafe(float x, float y);
	void Rotate(glm::vec3);
	NewCamera(Renderer* rnd);
	void SetOrtho();
	void SetPerspective();
	void Walk(int ammount);
	void StrafeAdvance(int ammount);
	void Pitch(float ammount);
	void Yaw(float ammount);
	void Roll(float ammount);
};
