#pragma once
#include "Exports.h"
#include "Componente.h"
#include "MaterialComponent.h"
#include "Shape.h"
#include "Definitions.h"
#include "ModelImporter.h"
#include "Model.h"
#include "TextureImporter.h"
#include "Transform.h"

class ENGINEDLL_API MeshRenderer :
	public Componente
{
protected:
	char* path;
	vector<MeshEntry> vecMeshEntry;
	vector<Header> vecHeaders;
	vector<unsigned int> bufferTextureID;
	Renderer* renderer;
	Transform* transform;
	Material* material;
public:
	MeshRenderer::MeshRenderer(Transform* _transform, Renderer* _renderer, Material* _material, string filepath = "cube.obj", string texturePath = "default.bmp");
	~MeshRenderer();
	void Update() override;
	void Draw();
};
