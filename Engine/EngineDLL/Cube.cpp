#include "Cube.h"
#define SIZE 10.0

Cube::Cube(Renderer* renderer, Material* material, Layers tag) :Shape(renderer,material,tag)
{
	verticesData = new float[24]
	{
	//front
	-SIZE, -SIZE, SIZE,
	SIZE, -SIZE, SIZE,
	SIZE, SIZE, SIZE,
	-SIZE, SIZE, SIZE,
	// back
	-SIZE, -SIZE, -SIZE,
	SIZE, -SIZE, -SIZE,
	SIZE, SIZE, -SIZE,
	-SIZE, SIZE, -SIZE
	};
	//antes era indexbufferid
	bufferId = SetVertices(verticesData, 8);

	colorVertex = new float[24]
	{
		// front colors
		1.0, 0.0, 0.0,
		0.0, 1.0, 0.0,
		0.0, 0.0, 1.0,
		1.0, 1.0, 1.0,
		// back colors
		1.0, 0.0, 0.0,
		0.0, 1.0, 0.0,
		0.0, 0.0, 1.0,
		1.0, 1.0, 1.0
	};
	SetColorVertices(colorVertex, 8);

	indxvertex = new unsigned int[36]
	{
		0, 1, 2,
		2, 3, 0,
		// right
		1, 5, 6,
		6, 2, 1,
		// back
		7, 6, 5,
		5, 4, 7,
		// left
		4, 0, 3,
		3, 7, 4,
		// bottom
		4, 5, 1,
		1, 0, 4,
		// top
		3, 2, 6,
		6, 7, 3
	};
	SetIndexVertices(indxvertex, 36);
	
	};


Cube::~Cube()
{
}

void Cube::DrawMesh(int type)
{
	renderer->loadIdentityMatrix();
	renderer->SetModelMatrix(model);

	if (material != NULL) {
		material->Bind();
		material->SetMatrixProperty("WVP", renderer->GetMVP());
	}
	renderer->EnableAttributes(0);
	renderer->EnableAttributes(1);
	renderer->BindBuffer(bufferId, 0);
	renderer->BindBuffer(colorBufferId, 1);
	renderer->BindMeshBuffer(indexBufferId);
	renderer->DrawIndex(indexVCount);
	renderer->DisableAttributes(0);
	renderer->DisableAttributes(1);
}

void Cube::Draw() 
{
	DrawMesh(GL_TRIANGLES);
	std::cout << "Drawing Cube" << std::endl;
}
