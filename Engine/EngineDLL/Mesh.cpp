
#include "Mesh.h"
#pragma warning(disable : 4996)


Mesh::Mesh(string filepath, string texturePath, Renderer* renderer, Material* material) : Shape(renderer, material, tag)
{
	path = new char[texturePath.size() + 1];
	texturePath.copy(path, texturePath.size() + 1);
	path[texturePath.size()] = '\0';

	Model::GetInstance()->LoadMesh(filepath, texturePath, vecMeshEntry, vecHeaders, renderer); //llama a loadMesh de la clase Model

	for (int i = 0; i < vecHeaders.size(); i++)
		bufferTextureID.push_back(renderer->GenTexture(vecHeaders[i].width, vecHeaders[i].height, vecHeaders[i].data));
}
Mesh::~Mesh()
{
}

void Mesh::UpdateModel()
{
	Shape::UpdateModel(); //llama al update de Shape.
}

void Mesh::Draw() //Good old Draw...
{
	renderer->loadIdentityMatrix();
	renderer->MultiplyModelMatrix(model);
	if (material != NULL) {
		material->Bind();
		material->SetMatrixProperty("MVP", renderer->GetMVP());
	}
	renderer->EnableAttributes(0);
	renderer->EnableAttributes(1);
	for (int i = 0; i < vecMeshEntry.size(); i++) {
		renderer->BindBuffer(vecMeshEntry[i].vertexBuffer, 0);
		renderer->BindTextureBuffer(vecMeshEntry[i].uvBuffer, 1);
		renderer->BindMeshBuffer(vecMeshEntry[i].indexBuffer);
		renderer->DrawIndex(vecMeshEntry[i].cantIndex);
	}
	renderer->DisableAttributes(0);
	renderer->DisableAttributes(1);
}