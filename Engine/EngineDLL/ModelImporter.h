#pragma once

#include "Exports.h"
#include "Renderer.h"
#include "GL\glew.h"
#include "glm\vec2.hpp"
#include "glm\vec3.hpp"
#include <vector>

using namespace std;
using namespace glm;

struct Vertex //struct Vertex para guardar todos los datos de cada vertice
{
	vec3 vPosition;
	vec2 vTexture;
	vec3 vNormal;

	Vertex() {}

	Vertex(const vec3& vp, const vec2& vt, const vec3& vn) //constructor para llenarlo en Model.cpp mas facilmente.
	{
		vPosition = vp;
		vTexture = vt;
		vNormal = vn;
	}
};

struct MeshEntry
{
	void Init(const vector<Vertex>& _vertex, const vector<unsigned int>& _index, Renderer* _renderer);

	unsigned int vertexBuffer;
	unsigned int indexBuffer;
	unsigned int uvBuffer;
	unsigned int cantIndex;

	MeshEntry();
	~MeshEntry();
};

