#pragma once
#include "Componente.h"
#include "Renderer.h"
#include "glm\exponential.hpp"
#include "glm\glm.hpp"
#include "glm\ext.hpp"
#include "glm\gtx\transform.hpp"
#include "Transform.h"
#include "Exports.h"
#define GLM_ENABLE_EXPERIMENTAL

using namespace glm;
class ENGINEDLL_API CameraComponent :
	public Componente
{
private:
	Renderer * renderer;
	vec3 eyePosition;
	vec3 cameraPosition;
	vec3 upVector;
	vec4 forward;
	vec4 right;
	vec4 up;
protected:
	Transform* transform;
public:
	void Strafe(float x, float y);
	void SetOrtho();
	void SetPerspective();
	void Walk(int ammount);
	void StrafeAdvance(int ammount);
	void Pitch(float ammount);
	void Yaw(float ammount);
	void Roll(float ammount);

	CameraComponent(Transform* _transform, Renderer* _renderer);
	void Update() override;
	~CameraComponent();
};

