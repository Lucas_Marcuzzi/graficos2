#include "ModelImporter.h"

MeshEntry::MeshEntry()
{
	vertexBuffer = NULL;
	indexBuffer = NULL;
	cantIndex = 0;
};

MeshEntry::~MeshEntry()
{
	if (vertexBuffer != NULL)
		glDeleteBuffers(1, &vertexBuffer);
	if (indexBuffer != NULL)
		glDeleteBuffers(1, &indexBuffer);
}

void MeshEntry::Init(const vector<Vertex>& vertex, const vector<unsigned int>& index, Renderer* rnd)
{
	cantIndex = index.size();

	float* meshPositions = new float[vertex.size() * 3];
	float* meshTextures = new float[vertex.size() * 2];
	float* meshNormals = new float[vertex.size() * 3];

	for (int i = 0; i < vertex.size(); i++)
	{
		meshPositions[i * 3] = vertex[i].vPosition.x;
		meshPositions[i * 3 + 1] = vertex[i].vPosition.y;
		meshPositions[i * 3 + 2] = vertex[i].vPosition.z;
		meshTextures[i * 2] = vertex[i].vTexture.x;
		meshTextures[i * 2 + 1] = vertex[i].vTexture.y;
		meshNormals[i * 3] = vertex[i].vNormal.x;
		meshNormals[i * 3 + 1] = vertex[i].vNormal.y;
		meshNormals[i * 3 + 2] = vertex[i].vNormal.z;
	}

	vertexBuffer = rnd->GenBuffer(meshPositions, sizeof(float) * vertex.size() * 3);
	indexBuffer = rnd->GenIndexBuffer(index);
	uvBuffer = rnd->GenBuffer(meshTextures, sizeof(float) * vertex.size() * 2);
}