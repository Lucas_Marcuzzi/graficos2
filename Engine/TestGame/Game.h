#pragma once
#include "GameBase.h"
#include "Triangle.h"
#include "Rectangle.h"
#include "Circle.h"
#include "Sprite.h"
#include "Tilemap.h"
#include "Input.h"
#include "CollisionManager.h"
#include "Cube.h"

class Game : public GameBase {

	Input* input;
	Material* material;
	Sprite* image;
	Tilemap* tiles;
	Rectangle* rect;
	Cube* dubidu;

public:
	Game();
	~Game();


protected:
	bool OnStart() override;
	bool OnStop() override;
	bool OnUpdate() override;
	bool OnDraw() override;

};
